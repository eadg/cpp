#include <iostream>

int main()
{
    int var1;
    int &rVar = var1;

    var1 = 5;
    std::cout << "var1: " << var1 << "\n";
    std::cout << "rVar: " << rVar << "\n";

    rVar = 7;
    std::cout << "var1: " << var1 << "\n";
    std::cout << "rVar: " << rVar << "\n";
    return 0;
}
