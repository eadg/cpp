#include<iostream>

class Prime {

    public:

       Prime() {
            std::cout << "...constructor..." << std::endl;
            pVal = new int(100);
        }

       Prime(int a) {
            std::cout << "...constructor..." << std::endl;
            pVal = new int(a);
        }

        ~Prime() {
            std::cout << "...destructor..." << std::endl;
            delete pVal;
        }

        int const get_val() {
            return *pVal;
        }

        void set_val(int val) {
            *pVal = val;
        }


    private:
        int *pVal;
};

int main() {

    int j;

    for(int i=1; i< 20; i++) {

        j = 2;
        for (; j<i ; j++) {

            if (i%j == 0) {
                break;
            }
        }
        if(i==j) {
            std::cout << "prime: " << i << std::endl;
        }
    }

    Prime *primus = new Prime;
    std::cout << "Primus: " << primus->get_val() << std::endl;
    primus->set_val(666);
    std::cout << "Primus: " << primus->get_val() << std::endl;
    delete primus;
    primus = new Prime;
    delete primus;
    primus = nullptr;
    primus = new Prime(2);
    std::cout << "Primus: " << primus->get_val() << std::endl;
    delete primus;
    primus = nullptr;
    delete primus;
    delete primus;
    primus = new Prime(20);
    std::cout << "Primus: " << primus->get_val() << std::endl;

  return 0;
}
